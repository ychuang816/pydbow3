#!/usr/bin/env bash

if [[ "$OSTYPE" == "linux-gnu" || "$OSTYPE" == "linux" ]]; then
    echo "Using system boost package"
    apt-get install -y libboost-all-dev
    exit
fi

# TODO: download and build libboost.python